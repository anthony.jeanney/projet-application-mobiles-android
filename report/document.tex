\documentclass{article}
\usepackage[a4paper, total={6in, 9in}]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{titlesec}
\usepackage[numbers]{natbib}

\title{Compte rendu de Projet Introduction au développement sous Android}
\author{
    Jeanney Anthony \\
    \texttt{anthony.jeanney@univ-littoral.fr} \\
    M2 WeDSci \\
    Université du Littoral Côte d'Opale \\
    Calais \\
}
\begin{document}

    \pagenumbering{arabic}
    \maketitle

	\renewcommand{\contentsname}{Sommaire}
    \tableofcontents

    \section{Introduction}\label{sec:introduction}

    Ce projet a pour but de créer une application Android qui permet à un utilisateur de répondre à des qcm enregistrés au sein de l'application et de suivre l'évolution de ses scores. Il doit également être possible de créer de nouveaux questionnaires à partir d'une partie administration de l'application.

    Le compte rendu suivant présente les différentes étapes de développement de l'application, les choix techniques effectués ainsi que les difficultés rencontrées.

    \section{Présentation de l'application}\label{sec:presentation}

    L'application est composée de 4 activités :
    \begin{itemize}
        \item \textbf{MainActivity} : activité principale de l'application, elle permet à l'utilisateur de choisir différents questionnaires, de consulter ses scores, de remettre à zéro ses scores et d'accéder à l'activité d'administration.
        \item \textbf{QuestionActivity} : activité permettant à l'utilisateur de répondre à un questionnaire.
        \item \textbf{ScoreActivity} : activité permettant à l'utilisateur de consulter ses scores.
        \item \textbf{CreateFormActivity} : activité permettant à l'utilisateur de créer de nouveaux questionnaires.
    \end{itemize}

    Les questionnaires sont stockés dans des fichiers texte avec le format suivant : la catégorie du questionnaire, suivie, pour chaque question, de son libellé et de ses réponses possibles, avec un x situé à la fin de l'un ou des choix correspondant à la ou les bonnes réponses.\\

    Lorsque l'utilisateur sélectionne un questionnaire, celui-ci présente les différentes questions successivement, dans un ordre aléatoire, et affiche les réponses possibles, ainsi que la catégorie, le numéro de la question et le nombre total de questions. L'utilisateur peut alors sélectionner une ou plusieurs réponses et valider sa réponse. L'application affiche alors une nouvelle question aléatoire jusqu'à ce que toutes les questions aient été posées. A la fin du questionnaire, l'application renvoie l'utilisateur vers l'activité principale.\\

    Après avoir répondu à un questionnaire, ce dernier devient indisponible pour l'utilisateur. Il peut alors consulter ses scores qui sont sauvegardés dans un fichier texte dans l'activité ScoreActivity. Il peut également remettre à zéro ses scores, ce qui rend tous les questionnaires disponibles à nouveau.\\

    Sur l'activité de consultation des scores, il est indiqué pour chaque questionnaire le score obtenu sur le nombre total de questions, ainsi que le score moyen obtenu sur l'ensemble des questionnaires sur 20.\\

    L'activité d'administration permet à l'utilisateur de créer de nouveaux questionnaires. L'utilisateur doit alors saisir le mot de passe d'administration pour y accéder. Il peut alors saisir le nom du questionnaire, sa catégorie, le nombre de questions, ainsi que les questions et réponses possibles, le tout sous le même format que les fichiers texte.\\

    Enfin, un menu d'options est disponible sur l'activité principale. Il permet à l'utilisateur de remettre à zéro ses scores, d'accéder à l'activité d'administration ou l'activité de consultation des scores, et de quitter l'application.

    \begin{figure}[h]
		\includegraphics[scale=0.30]{main_menu.png}
        \includegraphics[scale=0.30]{question.png}
        \includegraphics[scale=0.30]{scores.png}
		\caption{Activités principales de l'application}
		\label{figure1}
	\end{figure}

    \section{Choix techniques}\label{sec:choix}

    \subsection{Représentation et stockage des questionnaires}\label{subsec:representation}

    Il a été choisi de représenter les questionnaires par une classe \textbf{Form} qui contient une liste des questions, une liste des numéros des questions déjà posées, le score obtenu par l'utilisateur, la catégorie du questionnaire, le numéro de la question actuelle et l'identifiant du questionnaire.

    La classe \textbf{Question} contient le libellé de la question, la liste des réponses possibles et la liste des bonnes réponses.\\

    Afin de stocker les questionnaires dans des fichiers texte, une sérialisation et une désérialisation des questionnaires ont été implémentées.

    La sérialisation consiste à créer une chaîne de caractères similaires au format décrit dans la section \ref{sec:presentation} à partir d'un questionnaire. Cette chaîne de caractères est ensuite utilisée pour transmettre le questionnaire entre activités, ou pour l'enregistrer dans un fichier texte. La désérialisation va ensuite recréer un questionnaire à partir de cette chaîne de caractères.

    Cet enregistrement en fichier texte s'effectue dans un dossier \textbf{forms} pour les questionnaires et dans un dossier \textbf{scores} pour les scores. Les fichiers sont nommés en fonction de l'identifiant du questionnaire avec le format suivant : \textbf{form\{id\}.txt} pour les questionnaires et \textbf{score\{id\}.txt} pour les scores.

    Lors de la désérialisation, l'identifiant du questionnaire est récupéré à partir du nom du fichier texte.\\

    \subsection{Gestion dynamique des questionnaires}\label{subsec:dynamique}

    La difficulté principale de ce projet a été de gérer dynamiquement les questionnaires. En effet, le nombre de questionnaires peut varier, ainsi que le nombre de questions dans chaque questionnaire et le nombre de réponses possibles pour chaque question.\\

    Afin d'afficher dynamiquement les questionnaires dans l'activité principale, il a été choisi de créer une \textbf{ListView} qui contient des Objets \textbf{Form}. Cette dernière se voit attribuer un \textbf{setOnItemClickListener} qui permet de récupérer le questionnaire sélectionné par l'utilisateur et de l'envoyer à l'activité \textbf{QuestionActivity}.\\

    L'activité \textbf{QuestionActivity} récupère alors le questionnaire sélectionné et le désérialise. Elle affiche ensuite une question aléatoire et utilise une \textbf{ListView} avec un \textbf{android:choiceMode=} \textbf{"multipleChoice"} pour afficher les réponses possibles.

    Lorsque l'utilisateur valide sa réponse, l'activité récupère les réponses sélectionnées et les compare aux réponses attendues en utilisant la méthode \textbf{isCorrect} de la classe \textbf{Question}. Elle incrémente ensuite le score de l'utilisateur et affiche une nouvelle question aléatoire jusqu'à ce que toutes les questions aient été posées.\\

    L'activité \textbf{MainActivity} récupère ensuite l'identifiant du questionnaire et le score de l'utilisateur, afin d'ajouter ce dernier dans sa liste des Questionnaires. Elle va ensuite enregistrer le score de l'utilisateur pour le questionnaire correspondant dans un fichier texte, puis désactiver le questionnaire en utilisant la méthode \textbf{setEnable} sur un child de la \textbf{ListView}.\\

    L'activité \textbf{ScoreActivity} va quant à elle récupérer une liste des scores, une liste des scores maximaux (nombre de questions dans le questionnaire) et une liste des catégories des questionnaires. Elle va ensuite afficher ces informations dans un \textbf{TableLayout}. Pour cela, elle va créer dynamiquement des \textbf{TableRow} qui contiennent des \textbf{TextView}, la première colonne contenant la catégorie du questionnaire et la deuxième colonne contenant le score obtenu sur le nombre total de questions. Une dernière ligne est ensuite ajoutée pour afficher le score moyen obtenu sur l'ensemble des questionnaires.\\

    Enfin, lorsque que l'utilisateur confirme la création d'un nouveau questionnaire, l'activité \textbf{CreateFormActivity} va récupérer le texte et le renvoyer à l'activité \textbf{MainActivity} qui va le désérialiser et l'ajouter à sa liste des questionnaires et l'enregistrer dans un fichier texte.\\

    \subsection{Gestion de l'administration}\label{subsec:admin}

    Afin d'accéder à l'activité d'administration, l'utilisateur doit saisir le mot de passe d'administration. Ce dernier est demandé lors de la création de l'activité. Lorsque l'utilisateur valide le mot de passe, si celui-ci est correct, un autre \textbf{ConstraintLayout} est affiché et celui contenant le menu de selection de mot de passe est caché. Cela est possible en utilisant la méthode \textbf{setVisibility} et en passant en paramètre \textbf{View.GONE} pour cacher le layout et \textbf{View.VISIBLE} pour l'afficher.\\

    La zone de texte de saisie de questionnaires a été implémentées en utilisant un \textbf{NestedScrollView} et un \textbf{android:inputType="textMultiLine"}. Cela permet à l'utilisateur de saisir plusieurs lignes de texte et de faire défiller ou scroller la zone de texte.\\

    Il a également été nécessaire de définir un \textbf{setOnTouchListener} sur la zone de texte afin de pouvoir scroller la zone de texte sans avoir besoin de cliquer sur la zone de texte.\\

    Après avoir reçu le questionnaire, l'activité \textbf{MainActivity} va vérifer si celui-ci est valide en utilisant la méthode \textbf{isValid} de la classe \textbf{Form}. Si le questionnaire est valide, elle va procéder à l'ajout et à l'enregistrement.\\

    \begin{figure}[h]
        \centering
		\includegraphics[scale=0.30]{admin.png}
        \includegraphics[scale=0.30]{create_form.png}
		\caption{ConstraintLayout de l'activité d'administration avant et après la saisie du mot de passe}
		\label{figure2}
	\end{figure}

    \section{Conclusion}\label{sec:conclusion}

    Ce projet a permis de mettre en pratique les connaissances acquises lors des cours de développement sous Android. Les problèmes rencontrés ont aurons été intéressants à résoudre et auront permis d'approfondir les connaissances sur le développement Android et Java, avec notamment la gestion dynamique des questionnaires et la sérialisation et désérialisation des questionnaires.\\

    L'application est finalisée et fonctionnelle, mais il reste des possibilités d'amélioration, comme par exemple la possibilité de modifier le mot de passe d'administration, ou le stockage de ce dernier d'une manière plus sécurisée. Il serait également possible d'ajouter une fonctionnalité de suppression de questionnaires, ou encore de modifier les questionnaires existants.

\end{document}
