package com.example.projetjeanney;

import androidx.annotation.NonNull;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.util.ArrayList;

public class Form {
    private final ArrayList<Question> questions;
    private final ArrayList<Integer> questionsDone;
    private int score;
    private String category;
    private int currentQuestion;
    private final int index;

    public Form(int index) {
        this.questions = new ArrayList<>();
        this.questionsDone = new ArrayList<>();
        this.score = 0;
        this.category = "";
        this.currentQuestion = -1;
        this.index = index;
    }

    @NonNull
    @Override
    public String toString() {
        // used by the adapter to display the form in the list
        return this.category + " (" + (this.questions.size()) + " questions)";
    }

    public String serialize() {
        // serialize the form into a string

        // category is on the first line
        StringBuilder serialized = new StringBuilder(this.category + "\n\n");

        // for each question
        for (Question question : this.questions) {
            // question is on the first line and then there is an empty line
            serialized.append(question.getQuestion()).append("\n\n");

            // for each answers
            ArrayList<String> answers = question.getAnswers();
            for (int i = 0; i < answers.size(); i++) {
                // write the answer
                serialized.append(answers.get(i));
                // if the answer is correct, add a "x" at the end of the line
                if (question.getCorrectAnswers().contains(i)) {
                    serialized.append(" x");
                }
                serialized.append("\n");
            }
            serialized.append("\n");
        }
        return serialized.toString();
    }

    public void deserialize(String serialized) throws IOException {
        // deserialize the form from a string

        // clear current questions
        this.questions.clear();

        BufferedReader reader = new BufferedReader(new StringReader(serialized));

        // we read each line of the string
        String line;
        while ((line = reader.readLine()) != null) {

            // empty lines are ignored
            if (line.equals("")) {
                continue;
            }

            // the first line is the category
            if (this.category.equals("")) {
                this.category = line;
                continue;
            }

            // we read the question
            String question = line;
            reader.readLine(); // skip empty line

            // we read the lines of the answers until we reach an empty line
            ArrayList<String> answers = new ArrayList<>();
            ArrayList<Integer> correctAnswers = new ArrayList<>();
            while ((line = reader.readLine()) != null && !line.equals("")) {

                // we add the answer to the list and we check if it is correct
                String answer = line;
                if (answer.endsWith(" x")) {
                    // if the answer is correct, we remove the " x" at the end of the line
                    answer = answer.substring(0, answer.length() - 2);
                    // we add the index of the answer to the list of correct answers
                    correctAnswers.add(answers.size());
                }
                answers.add(answer);
            }
            // we add the question to the list of questions
            this.addQuestion(new Question(question, answers, correctAnswers));
        }

        // we randomize the first question
        randomizeQuestion();

        reader.close();
    }

    public void loadForm(FileInputStream form_file) throws IOException {
        // load the form from a file

        InputStreamReader reader = new InputStreamReader(form_file);
        BufferedReader bufferedReader = new BufferedReader(reader);

        // we read the file line by line and we add each line to a string
        StringBuilder serialized = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            serialized.append(line).append("\n");
        }

        // we deserialize the string
        this.deserialize(serialized.toString());

        bufferedReader.close();
        reader.close();
    }

    public void saveForm(FileOutputStream file) throws IOException {
        // save the form to a file
        OutputStreamWriter writer = new OutputStreamWriter(file);

        // we serialize the form and we write it to the file
        String serialized = this.serialize();
        writer.write(serialized);

        writer.close();
    }

    public void loadScore(FileInputStream file) throws IOException {
        // load the score from a file
        InputStreamReader reader = new InputStreamReader(file);
        BufferedReader bufferedReader = new BufferedReader(reader);

        // we read the score from the file and we parse it to an integer
        this.score = Integer.parseInt(bufferedReader.readLine());

        bufferedReader.close();
        reader.close();
    }

    public void saveScore(FileOutputStream file) throws IOException {
        // save the score to a file
        OutputStreamWriter writer = new OutputStreamWriter(file);

        // we write the score to the file
        writer.write(Integer.toString(this.score));

        writer.close();
    }

    private void randomizeQuestion() {
        // randomize the next question to one that has not been done
        int randomQuestion;
        do {
            randomQuestion = (int) (Math.random() * this.questions.size());
        } while (this.questionsDone.contains(randomQuestion));
        this.currentQuestion = randomQuestion;
        this.questionsDone.add(this.currentQuestion);
    }

    public void addQuestion(Question question) {
        this.questions.add(question);
    }

    public ArrayList<Question> getQuestions() {
        return this.questions;
    }

    public int getScore() {
        return this.score;
    }

    public String getCategory() {
        return this.category;
    }

    public Question getCurrentQuestion() {
        return this.questions.get(this.currentQuestion);
    }

    public int getCurrentQuestionIndex() {
        return this.currentQuestion;
    }

    public void nextQuestion() {
        // if all the questions have been done, we set the current question to -1
        if (this.questionsDone.size() == this.questions.size()) {
            this.currentQuestion = -1;
        } else {
            // otherwise we randomize the next question
            randomizeQuestion();
        }
    }

    public int getNumberOfQuestionsCompleted() {
        // return the number of questions that have been done
        return this.questionsDone.size();
    }

    public void incrementScore() {
        this.score++;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getIndex() {
        return this.index;
    }

    public boolean isValid() {
        // check if the form is valid
        for (Question question : this.questions) {
            // if the question is not valid, the form is not valid
            if (!question.isValid()) {
                return false;
            }
        }
        return !this.category.equals("");
    }
}
