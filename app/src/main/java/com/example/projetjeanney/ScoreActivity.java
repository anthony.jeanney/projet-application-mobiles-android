package com.example.projetjeanney;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class ScoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        setSupportActionBar(findViewById(R.id.title_bar));

        // retrieve necessary data from the intent
        int[] scores = getIntent().getIntArrayExtra("scores");
        int[] maxScores = getIntent().getIntArrayExtra("maxScores");
        String[] categories = getIntent().getStringArrayExtra("categories");

        // if we have all the data, display the scores
        if (scores != null && maxScores != null && categories != null) {
            displayScores(scores, maxScores, categories);
        }
    }

    @SuppressLint("StringFormatMatches")
    private void displayScores(int[] scores, int[] maxScores, String[] categories) {

        // get the table that will contain the scores
        TableLayout table = findViewById(R.id.table);

        // add new rows into the table for each score
        for (int i = 0; i < scores.length; i++) {
            TableRow row = createRow(String.format(getString(R.string.score_text), categories[i]),
                    String.format(getString(R.string.score_value), scores[i], maxScores[i]));
            table.addView(row);
        }

        // add a row for the mean score
        TableRow row = createRow(getString(R.string.mean_score),
                String.format(getString(R.string.score_value), getMeanScore(scores, maxScores), 20));
        table.addView(row);
    }

    private TableRow createRow(String left, String right) {
        // create a row with two columns
        TableRow row = new TableRow(this);
        row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));

        // left will contain the category name
        TextView category = new TextView(this);
        category.setText(left);
        category.setTextSize(16);
        category.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1));
        category.setPadding(10, 10, 10, 10);
        row.addView(category);

        // right will contain the score
        TextView score = new TextView(this);
        score.setText(right);
        score.setTextSize(16);
        score.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1));
        score.setPadding(10, 10, 10, 10);
        score.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
        row.addView(score);

        return row;
    }

    private double getMeanScore(int[] scores, int[] maxScores) {
        // compute the mean score
        double mean = 0;
        for (int i = 0; i < scores.length; i++) {
            mean += (double) scores[i] / maxScores[i];
        }
        // round to have only two decimals
        return Math.round(mean / scores.length * 20 * 100.0) / 100.0;
    }

    public void close(View view) {
        // close the activity when the close button is clicked
        finish();
    }
}