package com.example.projetjeanney;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class QuestionActivity extends AppCompatActivity {

    private Form form;
    private ArrayAdapter<String> adapter = null;
    private ListView listView = null;
    private Integer index;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        setSupportActionBar(findViewById(R.id.title_bar));

        // set the adapter for the answers list
        listView = findViewById(R.id.answers);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_multiple_choice);
        listView.setAdapter(adapter);

        // get the index of the form
        index = getIntent().getIntExtra("index", -1);

        // deserialize the form received from the main activity
        String serialized = getIntent().getStringExtra("serialized");
        form = new Form(-1);
        try {
            form.deserialize(serialized);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // display the first question
        updateQuestion();
    }

    private void updateQuestion() {
        // update the category
        TextView category = findViewById(R.id.category);
        category.setText(form.getCategory());

        // update the question number
        TextView question_number = findViewById(R.id.question_number);
        String formatted = String.format(getString(R.string.question_number), form.getNumberOfQuestionsCompleted(), form.getQuestions().size());
        question_number.setText(formatted);

        // update the question
        TextView question = findViewById(R.id.question);
        question.setText(form.getCurrentQuestion().getQuestion());

        // add all the answers to the list
        listView.clearChoices();
        adapter.clear();
        adapter.addAll(form.getCurrentQuestion().getAnswers());
        adapter.notifyDataSetChanged();
    }

    public void confirm(View view) {
        // get the selected answers
        ArrayList<Integer> selectedAnswers = new ArrayList<>();
        ListView listView = findViewById(R.id.answers);
        for (int i = 0; i < listView.getCount(); i++) {
            if (listView.isItemChecked(i)) {
                selectedAnswers.add(i);
            }
        }

        // check if the answers are correct
        if (form.getCurrentQuestion().isCorrect(selectedAnswers)) {
            form.incrementScore();  // increment the score
        }

        // go to the next question
        form.nextQuestion();

        // check if the form is finished
        if (form.getCurrentQuestionIndex() != -1) {
            // the form is not finished
            updateQuestion();  // update to display the next question
        } else {
            // the form is finished
            // send the results to the main activity
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra("index", index);
            intent.putExtra("score", form.getScore());
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    public void stop(View view) {
        // cancel the form when the button stop is clicked
        setResult(RESULT_CANCELED);
        finish();
    }
}