package com.example.projetjeanney;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Form> forms;
    private ArrayAdapter<Form> adapter = null;
    private ListView listView = null;
    private File formsDirectory = null;
    private File scoresDirectory = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(findViewById(R.id.title_bar));

        // create folders
        formsDirectory = createDirectory("forms");
        scoresDirectory = createDirectory("scores");

        // initialize the forms list
        this.forms = new ArrayList<>();
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, forms);

        // set the adapter for the forms list
        listView = findViewById(R.id.list_view);
        listView.setAdapter(adapter);
        // click on a form to start it
        listView.setOnItemClickListener((parent, view, position, id) -> {
            // if the form has already been completed, do nothing
            if (forms.get(position).getScore() > 0) {
                return;
            }
            // start the form
            Intent intent = new Intent(this, QuestionActivity.class);
            intent.putExtra("serialized", forms.get(position).serialize());
            intent.putExtra("index", forms.get(position).getIndex());
            launcherForm.launch(intent);
        });

        // load the forms from storage
        loadForms();

        // wait for list view to be loaded before disabling already completed forms
        listView.post(this::disableAlreadyCompletedForms);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // adding options to the menu
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater= getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }

    private final ActivityResultLauncher<Intent> launcherCreateForm = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
        result -> {
            if (result.getResultCode() == RESULT_OK && result.getData() != null) {
                // the form is created we get the data from the intent
                Intent intent = result.getData();

                // get the first available index
                int index = 0;
                for (Form form : this.forms) {
                    if (form.getIndex() >= index) {
                        index = form.getIndex() + 1;
                    }
                }

                Form form = new Form(index);

                // get the new form in serialized form and deserialize it
                String serialized = intent.getStringExtra("serialized");
                try {
                    form.deserialize(serialized);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }

                if (!form.isValid()) {
                    Toast.makeText(this, R.string.invalid_form, Toast.LENGTH_SHORT).show();
                    return;
                }

                // save the form
                try {
                    form.saveForm(new FileOutputStream(new File(formsDirectory, "form" + form.getIndex() + ".txt")));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }

                // add the form to the list
                this.forms.add(form);
                adapter.notifyDataSetChanged();
            }
        }
    );

    private final ActivityResultLauncher<Intent> launcherForm = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
        result -> {
            if (result.getResultCode() == RESULT_OK && result.getData() != null) {
                // the form is finished we get the data from the intent
                Intent intent = result.getData();

                // get the index of the form
                int index = intent.getIntExtra("index", -1);
                if (index == -1) {
                    throw new RuntimeException("No index received");
                }

                // get the score of the form
                int score = intent.getIntExtra("score", -1);
                if (score == -1) {
                    throw new RuntimeException("No score received");
                }

                // update the score of the form
                this.forms.get(index).setScore(score);

                // disable the forms that have already been completed
                disableAlreadyCompletedForms();

                // save the score
                try {
                    this.forms.get(index).saveScore(new FileOutputStream(new File(scoresDirectory, "scores" + index + ".txt")));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    );

    public void createForm(MenuItem item) {
        // start the create form activity when the button create form is clicked
        Intent intent = new Intent(this, CreateFormActivity.class);
        launcherCreateForm.launch(intent);
    }

    public void scoreDisplay(MenuItem item) {
        // start the score activity when the button score is clicked
        Intent intent = new Intent(this, ScoreActivity.class);

        // get the scores, max scores and categories of the forms
        int[] scores = new int[this.forms.size()];
        int[] maxScores = new int[this.forms.size()];
        String[] categories = new String[this.forms.size()];
        for (int i = 0; i < this.forms.size(); i++) {
            scores[i] = this.forms.get(i).getScore();
            maxScores[i] = this.forms.get(i).getQuestions().size();
            categories[i] = this.forms.get(i).getCategory();
        }
        intent.putExtra("scores", scores);
        intent.putExtra("maxScores", maxScores);
        intent.putExtra("categories", categories);

        startActivity(intent);
    }

    public void scoreReset(MenuItem item) {
        // reset the scores of all the forms when the button reset is clicked

        // for each form, set the score to 0 and enable the form in the list view
        for (int i = 0; i < this.forms.size(); i++) {
            this.forms.get(i).setScore(0);
            listView.getChildAt(i).setEnabled(true);
        }
        adapter.notifyDataSetChanged();

        // save the scores
        for (int i = 0; i < this.forms.size(); i++) {
            try {
                this.forms.get(i).saveScore(new FileOutputStream(new File(scoresDirectory, "scores" + i + ".txt")));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        Toast.makeText(this, "Scores réinitialisés", Toast.LENGTH_SHORT).show();
    }

    public void exit(MenuItem item) {
        // exit the application when the button exit is clicked
        finish();
    }

    private void loadForms() {
        // for each form file, load the form and the score
        for (File formFile : Objects.requireNonNull(formsDirectory.listFiles())) {
            // get the index of the form from the file name
            int index = Integer.parseInt(formFile.getName().substring(4, formFile.getName().length() - 4));
            // get the score file
            File scoresFile = new File(scoresDirectory, "scores" + index + ".txt");

            // create the form and load it
            Form form = new Form(index);
            try {
                form.loadForm(new FileInputStream(formFile));
                if (scoresFile.exists()) {
                    form.loadScore(new FileInputStream(scoresFile));
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            // add the form to the list
            this.forms.add(form);
        }
    }

    private void disableAlreadyCompletedForms() {
        // disable the forms that have already been completed
        for (int i = 0; i < this.forms.size(); i++) {
            if (this.forms.get(i).getScore() > 0) {
                listView.getChildAt(i).setEnabled(false);
            }
        }
    }

    private File createDirectory(String name) {
        // create the directory if it does not exist
        File directory = new File(getFilesDir(), name);
        if (!directory.exists()) {
            if (!directory.mkdir()) {
                throw new RuntimeException("Failed to create directory " + directory.getName());
            }
        }
        return directory;
    }

    private void deleteForm(int index) {
        // delete the form and its score
        Form form = this.forms.get(index);
        File file = new File(formsDirectory, "form" + form.getIndex() + ".txt");
        File scoresFile = new File(scoresDirectory, "scores" + form.getIndex() + ".txt");
        if (file.exists() && !file.delete()) {
            throw new RuntimeException("Failed to delete file " + file.getName());
        }
        if (scoresFile.exists() && !scoresFile.delete()) {
            throw new RuntimeException("Failed to delete file " + scoresFile.getName());
        }
        this.forms.remove(index);
        adapter.notifyDataSetChanged();
    }
}