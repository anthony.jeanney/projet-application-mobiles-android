package com.example.projetjeanney;

import java.util.ArrayList;

public class Question {
    private final String question;
    private final ArrayList<String> answers;
    private final ArrayList<Integer> correctAnswers;

    public Question(String question, ArrayList<String> answers, ArrayList<Integer> correctAnswers) {
        this.question = question;
        this.answers = answers;
        this.correctAnswers = correctAnswers;
    }
    
    public boolean isCorrect(ArrayList<Integer> answers) {
        // if the number of answers is not the same, the answer is wrong
        if (answers.size() != correctAnswers.size()) {
            return false;
        }
        // if the answers are not the same, the answer is wrong
        for (Integer answer : answers) {
            if (!correctAnswers.contains(answer)) {
                return false;
            }
        }
        return true;
    }

    public String getQuestion() {
        return this.question;
    }

    public ArrayList<String> getAnswers() {
        return this.answers;
    }

    public ArrayList<Integer> getCorrectAnswers() {
        return this.correctAnswers;
    }

    public boolean isValid() {
        // check if the question is valid
        return !this.question.equals("") && this.answers.size() > 1 && this.correctAnswers.size() > 0;
    }
}
