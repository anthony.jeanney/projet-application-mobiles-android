package com.example.projetjeanney;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CreateFormActivity extends AppCompatActivity {

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_form);
        setSupportActionBar(findViewById(R.id.title_bar));

        // Make text scrollable without keyboard open
        EditText text = (EditText) findViewById(R.id.form_text_input);
        text.setOnTouchListener((v, event) -> {
            if (v.getId() == R.id.form_text_input) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                if ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                }
            }
            return false;
        });
    }

    public void submit_form(View view) {
        // when clicking on the submit button, send the serialized form to the main activity
        String serialized = ((EditText)findViewById(R.id.form_text_input)).getText().toString();
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("serialized", serialized);
        setResult(RESULT_OK, intent);
        finish();
    }

    public void submit_password(View view) {
        // when clicking on the submit button for the password, check if the password is correct
        String password = ((TextView)findViewById(R.id.admin_password)).getText().toString();

        // if the password is correct, display the editor
        if (password.equals("MDP")) {
            setEditorVisible();
        } else {
            Toast.makeText(this, R.string.wrong_password, Toast.LENGTH_SHORT).show();
        }
    }

    public void exit(View view) {
        // when clicking on the exit button, go back to the main activity
        finish();
    }

    private void setEditorVisible() {
        // display the editor
        findViewById(R.id.login).setVisibility(View.GONE);
        findViewById(R.id.editor).setVisibility(View.VISIBLE);
    }
}